package eu.bindworks.mcourier.crypto;

import java.util.Objects;

public class ShipmentEncryptor {
    private ShipmentEncryptorConfiguration configuration;

    public ShipmentEncryptor(ShipmentEncryptorConfiguration configuration) {
        Objects.requireNonNull(configuration);

        this.configuration = configuration;
    }

    public EncryptedShipmentBuilder createShipment() {
        return new EncryptedShipmentBuilder(configuration);
    }

}
